import 'jquery.global.js';
import slick from 'slick-carousel';

let mainPage = {

  init: function() {
    mainPage.app = this;

    this.document.ready(() => {
      mainPage.initIndex();
    });
  },

  initIndex() {
    $('.js-cheaper').slick({
      infinite: false,
      variableWidth: true,
      slidesToScroll: 1,
      dots: true
    });

    $('.js-news-item-slider').slick({
      infinite: false,
      variableWidth: true,
      slidesToScroll: 1,
      dots: true
    });

    $('.js-recently-slider').slick({
      infinite: false,
      variableWidth: true,
      slidesToScroll: 1,
      dots: true
    });

    $('.js-same-model-slider').slick({
      infinite: false,
      variableWidth: true,
      slidesToScroll: 1,
      dots: true
    });

    $('.tabs__item').hover(function() {

      const tabs = $(this).closest('.tabs');
      const tabID = $(this).attr('data-tab');

      $('.tabs__content').removeClass('active');
      $('#' + tabID).addClass('active');

      tabs.find('.tabs__item').removeClass('active');
      $(this).addClass('active');
    });



    if ($(window).width() < 681) {
      $('.js-photo-inst').slick({
        infinite: false,
        variableWidth: true,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        responsive: [{
            breakpoint: 680,
            settings: {
              dots: false,
              arrows: false,
            },
          },

        ],
      });

      $('.js-sales-list').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        responsive: [{
            breakpoint: 680,
            settings: {
              dots: true,
              arrows: true,
            },
          },

        ],
      });
    }

    if ($(window).width() < 731) {
      $('.tabs__item').unbind('hover');

      $('.gold-main-headline h1').text('Каталог');
    }





    $('.checkbox-item').on('click', function() {
      if ($(this).is(':checked')) {
        $('.checkbox-item').not(this).prop('checked', false);
      }
    });


    const input = $('.input-item');

    input.on('focus', function() {
      $(this).parent().addClass('input-list-item--selected');
    });

    input.on('focusout', function() {
      if (!$(this).val().trim()) {
        $(this).parent().removeClass('input-list-item--selected');
      }
    });
  },

};

export default mainPage;