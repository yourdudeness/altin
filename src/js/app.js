import 'jquery.global.js';
import 'lazysizes';
// import Swiper from 'swiper/js/swiper.js';

import page from 'page';
import forms from 'forms';
import 'catalog.js';
import fancybox from '@fancyapps/fancybox';
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks
} from 'body-scroll-lock';

import basket from 'basket';
import mainPage from 'index.page';

import AOS from 'aos';


let app = {

  scrollToOffset: 200, // оффсет при скролле до элемента
  scrollToSpeed: 500, // скорость скролла

  init: function() {
    // read config
    if (typeof appConfig === 'object') {
      Object.keys(appConfig).forEach(key => {
        if (Object.prototype.hasOwnProperty.call(app, key)) {
          app[key] = appConfig[key];
        }
      });
    }

    app.currentID = 0;

    // Init page
    this.page = page;
    this.page.init.call(this);

    // Init page
    this.forms = forms;
    this.forms.init.call(this);

    this.basket = basket;
    this.basket.init.call(this);

    this.mainPage = mainPage;
    this.mainPage.init.call(this);

    //window.jQuery = $;
    window.app = app;

    app.document.ready(() => {

      AOS.init();

      const selectSingle = document.querySelector('.__select');
      const selectSingle_title = selectSingle.querySelector('.__select__title');
      const selectSingle_labels = selectSingle.querySelectorAll('.__select__label');

      // Toggle menu
      selectSingle_title.addEventListener('click', () => {
        if ('active' === selectSingle.getAttribute('data-state')) {
          selectSingle.setAttribute('data-state', '');
        } else {
          selectSingle.setAttribute('data-state', 'active');
        }
      });

      // Close when click to option
      for (let i = 0; i < selectSingle_labels.length; i++) {
        selectSingle_labels[i].addEventListener('click', (evt) => {
          selectSingle_title.textContent = evt.target.textContent;
          selectSingle.setAttribute('data-state', '');
        });
      }

      $('[data-fancybox], .js-modal').fancybox({
        toolbar: false,
        smallBtn: true,
        touch: false,
        autoFocus: false,

        afterLoad: function() {
          var fancyboxSlide = document.querySelectorAll(".fancybox-slide");
          fancyboxSlide.forEach(function(element) {
            // scrollLock.disablePageScroll(element);
            disableBodyScroll(element);
            blurBody(true);
          });
        },
        beforeClose: function() {
          blurBody(false);
          if ($('.fancybox-slide').length == 1) {
            // scrollLock.enablePageScroll();
            clearAllBodyScrollLocks();
          }
        },
      });

      function blurBody(blur) {
        if (blur) {
          $('.js-root').addClass('blur-active');
        } else {
          $('.js-root').removeClass('blur-active');
        }

      }

    });

    app.window.on('load', () => {
      const menuBtn = $('.js-menu-header');
      const menu = $('.btm-header-wrap');

      menuBtn.on('click', function(e) {
        e.stopPropagation();
        if ($(menu).hasClass('btm-header-wrap--active')) {
          closeMenu();
        } else {
          openMenu();
        }

      });

      function openMenu() {
        $('.btm-header-wrap').addClass('btm-header-wrap--active');
        disableBodyScroll(menu);
        menuBtn.addClass('btn-mob-menu-item--active');
      }

      function closeMenu() {
        $('.btm-header-wrap').removeClass('btm-header-wrap--active');
        clearAllBodyScrollLocks();
        menuBtn.removeClass('btn-mob-menu-item--active');
      }

      $('.free-shipping-toggle').on('click', function() {
        $(this).closest('.free-shipping-wrap').find('.free-shipping-hide-text').slideToggle(250);
      });


      $(window).scroll(function() {
        if ($(window).scrollTop() > 50) {
          $('.header-main').addClass('scroll-header');
        } else {
          $('.header-main').removeClass('scroll-header')
        }
      });


    });

    // this.document.on(app.resizeEventName, () => {
    // });

  },

};
app.init();